from flask import Flask, request, jsonify
from transformers import T5ForConditionalGeneration, T5Tokenizer

app = Flask(__name__)

model_name = "t5-small"
tokenizer = T5Tokenizer.from_pretrained(model_name)
model = T5ForConditionalGeneration.from_pretrained(model_name)


@app.route('/translate', methods=['POST'])
def translate():
    input_text = request.json.get('text', '')
    # Format the input as expected by the T5 model
    input_text = f"translate Chinese to Germany: {input_text}"

    inputs = tokenizer(input_text, return_tensors="pt", max_length=512, truncation=True)
    outputs = model.generate(**inputs, max_length=512)
    translated_text = tokenizer.decode(outputs[0], skip_special_tokens=True)
    return jsonify({"translated_text": translated_text})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)