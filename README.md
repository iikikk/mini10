# Mini10

## Requirements
Dockerize Hugging Face Rust transformer
Deploy container to AWS Lambda
Implement query endpoint

## Steps:
1. Create app.py that could translate English to Germany.
2. Create Dockerfile.
3. Build docker with the following command.
docker build -t generate_app .
4. Naviagte into ECR under AWS console and create a new private repository.
5. tag the container with the following command:
docker tag generate:latest 851725176910.dkr.ecr.us-east-1.amazonaws.com/mini10
6. docker push with the following command:
docker push 851725176910.dkr.ecr.us-east-1.amazonaws.com/mini10
7. Deploy container to AWS Lambda.
8. Testing the URL with postman.

## Screenshot：
### ECR:
 ![Alt text](image-2.png)
 ![Alt text](image-3.png)
 
### Lambda：
 ![Alt text](image-1.png)

### URL Testing:
 ![Alt text](image.png)
